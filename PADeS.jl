__precompile__()

module PADeS

using DifferentialEquations: ODEProblem, MonteCarloProblem, solve
using NLsolve: nlsolve

const rng = MersenneTwister(42)

function SNBSproblem(prob,
        num_monte::Int, 
        batch_size::Int,
        timespan::Tuple{Number,Number},
        node_list::Array{Int,1};
        pert_strength=(pi, 10),
        tol=1e-1,
        ptype=:split_threads
    )
    
    @assert length(node_list) == num_monte
    
    function rcg_snbs(prob,i,repeat)
        node = node_list[i]
        prob.u0[node] += pert_strength[1] * randn(rng, Float64)
        prob.u0[prob.p.n+node] += pert_strength[2] * randn(rng, Float64)
        #--#
        ODEProblem(prob.f,
            prob.u0,
            prob.tspan, 
            prob.p
        )
    end
    
    naive_convergence(maxabsf) = maxabsf < tol ? true : false
    
    function sum_convergence(u,data,I)
        reduced = mean(map(x -> naive_convergence(x), data))
        #--#
        (append!(u, reduced), false)
    end
    
    monte_prob = MonteCarloProblem(prob,
        output_func=observer_fin_abs_f, 
        prob_func=rcg_snbs, 
        reduction=sum_convergence
        )
    
    #--#
    solve(monte_prob, save_everystep=false, num_monte=num_monte, batch_size=batch_size, parallel_type=ptype), monte_prob
end

export SNBSproblem

function ASBSproblem(prob,
        num_monte::Int, 
        timespan::Tuple{Number,Number};
        pert_strength=(pi, 10),
        tol=1e-1,
        ptype=:split_threads
    )
    
    function rcg_asbs(prob,i,repeat)
        node = rand(1:prob.p.n)
        prob.u0[node] += pert_strength[1] * randn(rng, Float64)
        prob.u0[prob.p.n+node] += pert_strength[2] * randn(rng, Float64)
        #--#
        ODEProblem(prob.f,
            prob.u0,
            prob.tspan, 
            prob.p
        )
    end
    
    naive_convergence(maxabsf) = maxabsf < tol ? true : false
    
    function sum_convergence(u,data,I)
        reduced = mean(map(x -> naive_convergence(x), data))
        #--#
        (append!(u, reduced), false)
    end

    monte_prob = MonteCarloProblem(prob,
        output_func=observer_fin_abs_f, 
        prob_func=rcg_asbs, 
        reduction=sum_convergence
        )
    
    #--#
    solve(monte_prob, save_everystep=false, num_monte=num_monte, batch_size=num_monte, parallel_type=ptype), monte_prob
end

export ASBSproblem

function get_root(f, p, guess)
    rfunc! = (dx, x) -> f(dx, x, p, 0)
    erg = nlsolve(rfunc!, guess)
    #--#
    erg.zero
end

export get_root

function observer_fin_abs_f(sol,i)
    n = sol.prob.p.n
    #--#
    (maximum(abs, sol[end-n+1:end, end]), false)
end

end