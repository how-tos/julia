# Parallel Julia

In order to make Julia work in parallel on the cluster you need to use the ClusterManagers package. First a regular SLURM bash script like the one in this folder allocates the recourses that you want. Julia is then called without any extra options, so no --machinefile or -n XYZ as you will see in some other examples on websites. These will not work on the PIK cluster because they rely on SSH for initializing new processes. Within the Julia script ClusterManagers is then used to initialize all processes that you want. Subsequently all parallel Julia operations that are documented here: https://docs.julialang.org/en/stable/manual/parallel-computing/ should work.

In this folder you will find a minimal example doing a Parallel MonteCarloProblem from the DifferentialEquations library in order to solve a Roessler System for many different initial conditions.

## Requirements for the example

The example requires the following Julia packages:
* ClusterManagers
* JLD2
* FileIO
* DifferentialEquations

## To-Do / Bugs

* the ```@everywhere using ... ``` often leads to warnings that the module is being reloaded. While some people say that the @everywhere is not needed at all, the code does often not work without it. An alternative suggested by other people is: 
```
import ....     (only on the master process)
@everywhere using .... (declares it on all processes)
```
* do it with a better I/O system than JLD2
* add a simple analysis example
