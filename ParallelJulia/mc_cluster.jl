# Julia Script to demonstrate a MonteCarloProblem solved in parallel on the cluster
#
# This is supposted to be called from the command line via a slurm script with an extra argument specifying the amount of processes that
# can be allocated

using ClusterManagers
N_tasks = parse(Int, ARGS[1])
N_worker = N_tasks
addprocs(SlurmManager(N_worker))

using JLD2, FileIO
@everywhere using DifferentialEquations

# We define a data structure that has all the dynamical parameters we care about.
@everywhere struct roessler_parameters
   a
   b
   c
end

#We define our dynamical system with in place notation. The variable du is overwritten with the derivatives.
# p is the parameters, ::roessler_parameters means it is of type roessler_parameters.
@everywhere function roessler(du, u, p::roessler_parameters, t)
   du[1] = - u[2] - u[3]
   du[3] = p.b + u[3] * (u[1] - p.c)
   du[2] = u[1] + p.a * u[2]
end

pars = roessler_parameters(0.32, 0.3, 4.5)
rp = ODEProblem(roessler, rand(3), (0.,2000.), pars)

# For MonteCarlo simulations we have to build a new problem every time. This takes a base problem,
# and gives back a new one, to illustrate, the following just gives back the same problem:
@everywhere rand_ic_problem = (prob,i,repeat) -> ODEProblem(prob.f, rand(3), prob.tspan, prob.p)
# The parameter prob is the base problem we are varying.
# The parameter i is the index of the Monte Carlo run we are generating the problem for.
# The repeat parameter indicates whether we are rerunning an experiment. Ignore it for now.

# define the MC Problem
mcp = MonteCarloProblem(rp, prob_func=rand_ic_problem)

# solve it, the :pmap symbol defines the way the problem is parallized by julia
sol_mc = @time solve(mcp, num_monte=100, parallel_type=:pmap)

# save the solutions, analyze them later
@save "mc_roes_test.jld2" sol_mc
