#!/bin/bash

#SBATCH --qos=short
#SBATCH --job-name=julia-mc-test
#SBATCH --account=YOURGROUP
#SBATCH --output=julia-mc-test-%j-%N.out
#SBATCH --error=julia-mc-test-%j-%N.err
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=8
#SBATCH --workdir=/p/tmp/YOURUSERNAME
#SBATCH --mail-type=END
#SBATCH --mail-user=YOURUSERNAME@pik-potsdam.de

echo "------------------------------------------------------------"
echo "SLURM JOB ID: $SLURM_JOBID"
echo "$SLURM_NTASKS tasks"
echo "------------------------------------------------------------"

module load julia
module load hpc
julia /p/tmp/YOURUSERNAME/julia-test/mc_cluster.jl $SLURM_NTASKS
